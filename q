#!/usr/bin/env bash

PROJECT_PATH="projects/${1}"
#WWWGROUP=1000
#WWWUSER=1000
APP_PORT=80

docker build -t frickelbase/quasar .

if [[ -z "${1}" ]]
then
  cmd="cd projects && quasar create"
else
  if [[ "-c" == "${1}" ]]
  then
    cmd="${@:2}"
  elif [[ "new" == "${2}" ]]
  then
    cmd="cd projects && quasar create ${1} ${@:3} && cd ${1} && quasar dev"
  elif [[ "-c" == "${2}" ]]
  then
    cmd="cd ${PROJECT_PATH} && ${@:3}"
  else
    cmd="cd ${PROJECT_PATH} && quasar ${@:2}"
  fi
fi

docker run -it \
  --pid=host \
  -v ${PWD}:/app \
  -p ${APP_PORT}:8080 \
  --rm \
  --user=node \
  --name quasar \
  frickelbase/quasar \
  bash -c "$cmd"
