## Available Commands

Create a new project named `example`:
```bash
./q example new
./q example new --branch next # Quasar beta
```

Serve project at [localhost](http://localhost:80) using Docker:
```bash
./q example
```

Build the app:
```bash
./q example build prod
```

Any command with quasar:
```bash
./q example any command like 'dev' or 'build prod'
```

Any command in shell:
```bash
./q example -c ls -lah
```

Run any command in root folder. For example to create a new extension use:
```bash
./q -c 'cd extensions && quasar create my-ext-name --kit app-extension'
./q -c 'cd extensions && quasar create my-ext-name --kit ui'
./q -c 'cd projects/example && yarn add --dev link:/app/extensions/extension-name'
```
