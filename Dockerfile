FROM node:lts-buster

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get install bash

RUN npm install -g @quasar/cli git

WORKDIR /app

RUN chown -R 1000:1000 /app

#RUN groupadd --force -g $WWWGROUP node
#RUN useradd -ms /bin/bash --no-user-group -g $WWWGROUP -u $WWWUSER node

EXPOSE 8080

ENTRYPOINT ["/usr/bin/env"]